<?php
/**
 * @file
 * Template file for vbo_browse block content.
 */
?>

<?php
  $css_path = '/css/vbo_browse_block_content.css';
  drupal_add_css(drupal_get_path('module', 'vbo_browse') . $css_path);
?>

<div class="vbo-browse-pager">
  <span class="vbo-browse-first">
    <?php if (isset($first) && $first['id'] != $current['id']): ?>
      <?php
        $first['uri']['options']['attributes']['title'] = t("Go to first") . ': '
          . $first['entity']->title;
      ?>
      <?php print l(t("<<"), $first['uri']['path'], $first['uri']['options']); ?>
    <?php else: ?>
      <?php print "<a>&lt;&lt;</a>"; ?>
    <?php endif; ?>
  </span>

  <span class="vbo-browse-prev">
    <?php if (isset($prev)): ?>
      <?php
        $prev['uri']['options']['attributes']['title'] = t("Go to previous") . ': '
          . $prev['entity']->title;
      ?>
      <?php print l(t("<"), $prev['uri']['path'], $prev['uri']['options']); ?>
    <?php else: ?>
      <?php print "<a>&lt;</a>"; ?>
    <?php endif; ?>
  </span>

  <span class="vbo-browse-current">
    <?php print $offset + 1 . " of " . count($entities); ?>
  </span>

  <span class="vbo-browse-next">
    <?php if (isset($next)): ?>
      <?php
        $next['uri']['options']['attributes']['title'] = t("Go to next") . ': '
          . $next['entity']->title;
      ?>
      <?php print l(t(">"), $next['uri']['path'], $next['uri']['options']); ?>
    <?php else: ?>
      <?php print "<a>&gt;</a>"; ?>
    <?php endif; ?>
  </span>

  <span class="vbo-browse-last">
    <?php if (isset($last) && $last['id'] != $current['id']): ?>
      <?php
        $last['uri']['options']['attributes']['title'] = t("Go to last") . ': '
          . $last['entity']->title;
      ?>
      <?php print l(t(">>"), $last['uri']['path'], $last['uri']['options']); ?>
    <?php else: ?>
      <?php print "<a>&gt;&gt;</a>"; ?>
    <?php endif; ?>
  </span>
</div>
<div class="vbo-browse-view-uri">
  <a href="<?php print $view_uri; ?>">
    <?php print t("Go back to results"); ?>
  </a>
</div>
