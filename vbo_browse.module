<?php

/**
 * @file
 * VBO Browse main module file.
 */

/**
 * Implements hook_action_info().
 */
function vbo_browse_action_info() {
  $actions = array(
    'vbo_browse_action' => array(
      'type' => 'entity',
      'label' => t("Browse entities"),
      'configurable' => FALSE,
      'triggers' => array('any'),
      'aggregate' => TRUE,
    ),
  );

  return $actions;
}

/**
 * Action callback.
 *
 * Store entities into session and redirect to the first entity.
 */
function vbo_browse_action($entities, $context = array()) {
  if (isset($_SESSION)) {
    unset($_SESSION['vbo_browse']);
  }

  $entity_type = $context['entity_type'];
  if (!empty($entities)) {
    $_SESSION['vbo_browse']['entities'] = $entities;
    $_SESSION['vbo_browse']['entity_type'] = $entity_type;
    $_SESSION['vbo_browse']['view_uri'] = request_uri();
    $entity = reset($entities);
    $uri = entity_uri($entity_type, $entity);
    if (isset($uri)) {
      drupal_goto($uri['path'], $uri['options']);
    }
    else {
      $ids = entity_extract_ids($entity_type, $entity);
      $args = array('entity_id' => $ids[0], 'entity_type' => $entity_type);
      $message = t("No URI found for this entity id: @entity_id (type: @entity_type).", $args);
      drupal_set_message($message, 'error');
    }
  }
  else {
    drupal_set_message(t("No entities to browse."), 'error');
  }
}

/**
 * Implements hook_block_info().
 */
function vbo_browse_block_info() {
  $blocks = array(
    'vbo_browse' => array(
      'info' => t('VBO Browse'),
      'cache' => DRUPAL_CACHE_PER_PAGE,
    ),
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function vbo_browse_block_view($delta = '') {
  $block = array();

  if ($delta == 'vbo_browse' && isset($_SESSION['vbo_browse']['entities'])) {
    $entities = $_SESSION['vbo_browse']['entities'];
    $entity_type = $_SESSION['vbo_browse']['entity_type'];
    $entities_ids = array_keys($entities);

    // Get the entity being viewed.
    $entity = menu_get_object($entity_type);
    if (isset($entity)) {
      $ids = entity_extract_ids($entity_type, $entity);
      $offset = array_search($ids[0], $entities_ids);

      if ($offset !== FALSE) {
        // If this entity is in the list of entities selected with VBO,
        // build "Previous" and "Next" links.
        $prev_uri = NULL;
        $next_uri = NULL;

        $offsets = array(
          'first' => 0,
          'prev' => $offset - 1,
          'current' => $offset,
          'next' => $offset + 1,
          'last' => count($entities_ids) - 1,
        );

        $variables = array(
          'view_uri' => $_SESSION['vbo_browse']['view_uri'],
          'entities' => $entities,
          'entity_type' => $entity_type,
          'offset' => $offset,
        );

        foreach ($offsets as $key => $offset) {
          if (isset($entities_ids[$offset])) {
            $id = $entities_ids[$offset];
            $entity = $entities[$id];
            $uri = entity_uri($entity_type, $entity);

            $variables[$key] = array(
              'id' => $id,
              'entity' => $entity,
              'uri' => $uri,
            );
          }
        }

        // Finally render the block.
        $block['subject'] = t('Browse results');

        $theme_name = "vbo_browse_block_content__$entity_type";
        $block['content'] = theme($theme_name, $variables);
      }
    }
  }

  return $block;
}

/**
 * Implements hook_theme().
 */
function vbo_browse_theme($existing, $type, $theme, $path) {
  $themes = array(
    'vbo_browse_block_content' => array(
      'variables' => array(
        'first_uri' => NULL,
        'prev_uri' => NULL,
        'next_uri' => NULL,
        'last_uri' => NULL,
      ),
      'template' => 'vbo-browse-block-content',
      'path' => drupal_get_path('module', 'vbo_browse') . '/templates',
    ),
  );

  return $themes;
}
